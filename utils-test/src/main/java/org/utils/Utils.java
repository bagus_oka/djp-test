package org.utils;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import sun.security.pkcs.ContentInfo;
import sun.security.pkcs.PKCS7;
import sun.security.pkcs.SignerInfo;
import sun.security.util.DerOutputStream;
import sun.security.util.DerValue;
import sun.security.x509.AlgorithmId;
import sun.security.x509.X500Name;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

public class Utils {

    public Utils() {
    }

    public static String signData(String message, String passphrase, String certificatePath, String alias) throws Exception {
        byte[] data = message.getBytes();
        Security.addProvider(new BouncyCastleProvider());
        char[] keystorePassword = passphrase.toCharArray();
        char[] keyPassword = passphrase.toCharArray();
        KeyStore keystore = KeyStore.getInstance("PKCS12");
        keystore.load(new FileInputStream(certificatePath), keystorePassword);
        Certificate[] certs = keystore.getCertificateChain(alias);
        X509Certificate[] certChain = new X509Certificate[certs.length];

        for(int i = 0; i < certs.length; ++i) {
            certChain[i] = (X509Certificate)certs[i];
        }

        X509Certificate userCert = (X509Certificate)keystore.getCertificate(alias);
        PrivateKey key = (PrivateKey)keystore.getKey(alias, keyPassword);
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initSign(key);
        signature.update(data);
        byte[] signedData = signature.sign();
        AlgorithmId digestAlgorithmId = new AlgorithmId(AlgorithmId.SHA256_oid);
        AlgorithmId signAlgorithmId = new AlgorithmId(AlgorithmId.RSAEncryption_oid);
        SignerInfo sInfo = new SignerInfo(X500Name.asX500Name(userCert.getSubjectX500Principal()), userCert.getSerialNumber(), digestAlgorithmId, signAlgorithmId, signedData);
        ContentInfo cInfo = new ContentInfo(ContentInfo.DATA_OID, (DerValue)null);
        PKCS7 p7 = new PKCS7(new AlgorithmId[]{digestAlgorithmId}, cInfo, (X509Certificate[])null, new SignerInfo[]{sInfo});
        ByteArrayOutputStream bOut = new DerOutputStream();
        p7.encodeSignedData(bOut);
        byte[] encoded = bOut.toByteArray();
        SignatureDataType signDT = new SignatureDataType(encoded, certChain);
        return DatatypeConverter.printBase64Binary(toBytes(signDT));
    }

    public static String signData(String message, PrivateKey privateKey, X509Certificate userCert) throws Exception {
        byte[] data = message.getBytes();
        Security.addProvider(new BouncyCastleProvider());
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initSign(privateKey);
        signature.update(data);
        byte[] signedData = signature.sign();
        AlgorithmId digestAlgorithmId = new AlgorithmId(AlgorithmId.SHA256_oid);
        AlgorithmId signAlgorithmId = new AlgorithmId(AlgorithmId.RSAEncryption_oid);
        SignerInfo sInfo = new SignerInfo(X500Name.asX500Name(userCert.getSubjectX500Principal()), userCert.getSerialNumber(), digestAlgorithmId, signAlgorithmId, signedData);
        ContentInfo cInfo = new ContentInfo(ContentInfo.DATA_OID, (DerValue)null);
        PKCS7 p7 = new PKCS7(new AlgorithmId[]{digestAlgorithmId}, cInfo, (X509Certificate[])null, new SignerInfo[]{sInfo});
        ByteArrayOutputStream bOut = new DerOutputStream();
        p7.encodeSignedData(bOut);
        byte[] encoded = bOut.toByteArray();
        SignatureDataType signDT = new SignatureDataType(encoded, new X509Certificate[]{userCert});
        return DatatypeConverter.printBase64Binary(toBytes(signDT));
    }

    public static String hashData(String message) throws Exception {
        String hashAlgorithm = "MD5";
        MessageDigest m = MessageDigest.getInstance(hashAlgorithm);
        m.reset();
        m.update(message.toString().getBytes());
        byte[] approvalByte = m.digest();
        String approvalCode = (new BigInteger(1, approvalByte)).toString(16);
        return approvalCode;
    }

    public static byte[] toBytes(Object obj) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        byte[] bytes;
        try {
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(obj);
            oos.flush();
            oos.reset();
            bytes = baos.toByteArray();
            oos.close();
            baos.close();
        } catch (IOException var4) {
            bytes = new byte[0];
        }

        return bytes;
    }
}
