package org.utils;

import java.io.Serializable;
import java.security.cert.X509Certificate;

public class SignatureDataType implements Serializable {

    private static final long serialVersionUID = 8425471360342991815L;
    byte[] p7Signature;
    X509Certificate[] certificateChain;

    public SignatureDataType(byte[] p7Signature, X509Certificate[] certificateChain) {
        this.p7Signature = p7Signature;
        this.certificateChain = certificateChain;
    }

    public void setP7Signature(byte[] p7Signature) {
        this.p7Signature = p7Signature;
    }

    public void setCertificateChain(X509Certificate[] certificateChain) {
        this.certificateChain = certificateChain;
    }

    public byte[] getP7Signature() {
        return this.p7Signature;
    }

    public X509Certificate[] getCertificateChain() {
        return this.certificateChain;
    }
}
