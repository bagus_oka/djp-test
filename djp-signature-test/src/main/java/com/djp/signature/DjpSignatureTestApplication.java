package com.djp.signature;

import lombok.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.utils.Utils;

import java.io.*;
import java.math.BigDecimal;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;

@SpringBootApplication
public class DjpSignatureTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(DjpSignatureTestApplication.class, args);
    }

}

@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
class TestController {

    private final CertificateService certificateService;

    @PutMapping("/lampiranAB")
    public DataSignature put(
        @RequestParam(name = "npwp") String npwp,
        @RequestParam(name = "passphrase") String passphrase,
        @RequestBody SptLampiranAB sptLampiranAB
    ) throws Exception {
        CertificateDetails certDetails = this.certificateService.getClientCertificateDetails(npwp, passphrase);
        String message = SignGenerator.generateSignatureLampiranAB(sptLampiranAB);
        String signature = Utils.signData(message, certDetails.getPrivateKey(), certDetails.getCertificate());
        String hash = Utils.hashData(message);
        DataSignature data = new DataSignature();
        data.setCertSn(certDetails.getCertificate().getSerialNumber().toString());
        data.setHash(hash);
        data.setSignature(signature);
        return data;
    }

    @PutMapping("/dokumenPK")
    public DataSignature put(
        @RequestParam(name = "npwp") String npwp,
        @RequestParam(name = "passphrase") String passphrase,
        @RequestBody ReqDokumenPk reqDokumen
    ) throws Exception {
        CertificateDetails certDetails = this.certificateService.getClientCertificateDetails(npwp, passphrase);
        String message = SignGenerator.generateSignatureDokumenPk(reqDokumen);
        String signature = Utils.signData(message, certDetails.getPrivateKey(), certDetails.getCertificate());
        String hash = Utils.hashData(message);
        DataSignature data = new DataSignature();
        data.setCertSn(certDetails.getCertificate().getSerialNumber().toString());
        data.setHash(hash);
        data.setSignature(signature);
        return data;
    }
}

@Getter
@Setter
class DataSignature {

    String signature;
    String hash;
    String certSn;
}

@Getter
@Setter
class SptLampiranAB {

    private String attribute1;
    private String attribute2;
    private BigDecimal attribute3;
    private BigDecimal attribute4;
    private BigDecimal attribute5;
    private BigDecimal attribute6;
    private BigDecimal attribute7;
    private BigDecimal attribute8;
    private BigDecimal attribute9;
    private BigDecimal attribute10;
    private BigDecimal attribute11;
    private BigDecimal attribute12;
    private BigDecimal attribute13;
    private BigDecimal attribute14;
    private BigDecimal attribute14P;
    private BigDecimal attribute15;
    private BigDecimal attribute15P;
    private BigDecimal attribute16;
    private BigDecimal attribute16P;
    private BigDecimal attribute17;
    private BigDecimal attribute18;
    private BigDecimal attribute19;
    private BigDecimal attribute20;
    private BigDecimal attribute21;
    private BigDecimal attribute22;
    private BigDecimal attribute23;
    private BigDecimal attribute24;
    private BigDecimal attribute25;
    private BigDecimal attribute26;
    private BigDecimal attribute27;
    private BigDecimal attribute28;
    private BigDecimal attribute29;
    private BigDecimal attribute30;
    private BigDecimal attribute31;
    private BigDecimal attribute32;
    private BigDecimal attribute33;
    private BigDecimal attribute34;
    private BigDecimal attribute35;
    private BigDecimal attribute36;
    private BigDecimal attribute37;
    private BigDecimal attribute38;
    private BigDecimal attribute39;
    private String attribute40;
    private String attribute41;
    private BigDecimal attribute42;
    private BigDecimal attribute43;
    private BigDecimal attribute44;
    private BigDecimal attribute45;
    private String signature;
    private String certSn;
}

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class SignGenerator {

    static String generateSignatureLampiranAB(SptLampiranAB sptLampiranAB) {
        return "attribute1=" + sptLampiranAB.getAttribute1() + ":attribute2=" + sptLampiranAB.getAttribute2() + ":attribute3=" + sptLampiranAB.getAttribute3() + ":attribute4=" + sptLampiranAB.getAttribute4() + ":attribute5=" + sptLampiranAB.getAttribute5() + ":attribute6=" + sptLampiranAB.getAttribute6() + ":attribute7=" + sptLampiranAB.getAttribute7() + ":attribute8=" + sptLampiranAB.getAttribute8() + ":attribute9=" + sptLampiranAB.getAttribute9() + ":attribute10=" + sptLampiranAB.getAttribute10() + ":attribute11=" + sptLampiranAB.getAttribute11() + ":attribute12=" + sptLampiranAB.getAttribute12() + ":attribute13=" + sptLampiranAB.getAttribute13() + ":attribute14=" + sptLampiranAB.getAttribute14() + ":attribute14P=" + sptLampiranAB.getAttribute14P() + ":attribute15=" + sptLampiranAB.getAttribute15() + ":attribute15P=" + sptLampiranAB.getAttribute15P() + ":attribute16=" + sptLampiranAB.getAttribute16() + ":attribute16P=" + sptLampiranAB.getAttribute16P() + ":attribute17=" + sptLampiranAB.getAttribute17() + ":attribute18=" + sptLampiranAB.getAttribute18() + ":attribute19=" + sptLampiranAB.getAttribute19() + ":attribute20=" + sptLampiranAB.getAttribute20() + ":attribute21=" + sptLampiranAB.getAttribute21() + ":attribute22=" + sptLampiranAB.getAttribute22() + ":attribute23=" + sptLampiranAB.getAttribute23() + ":attribute24=" + sptLampiranAB.getAttribute24() + ":attribute25=" + sptLampiranAB.getAttribute25() + ":attribute26=" + sptLampiranAB.getAttribute26() + ":attribute27=" + sptLampiranAB.getAttribute27() + ":attribute28=" + sptLampiranAB.getAttribute28() + ":attribute29=" + sptLampiranAB.getAttribute29() + ":attribute30=" + sptLampiranAB.getAttribute30() + ":attribute31=" + sptLampiranAB.getAttribute31() + ":attribute32=" + sptLampiranAB.getAttribute32() + ":attribute33=" + sptLampiranAB.getAttribute33() + ":attribute34=" + sptLampiranAB.getAttribute34() + ":attribute35=" + sptLampiranAB.getAttribute35() + ":attribute36=" + sptLampiranAB.getAttribute36() + ":attribute37=" + sptLampiranAB.getAttribute37() + ":attribute38=" + sptLampiranAB.getAttribute38() + ":attribute39=" + sptLampiranAB.getAttribute39() + ":attribute40=" + sptLampiranAB.getAttribute40() + ":attribute41=" + sptLampiranAB.getAttribute41() + ":attribute42=" + sptLampiranAB.getAttribute42() + ":attribute43=" + sptLampiranAB.getAttribute43() + ":attribute44=" + sptLampiranAB.getAttribute44() + ":attribute45=" + sptLampiranAB.getAttribute45();
    }

    static String generateSignatureDokumenPk(ReqDokumenPk dokumenPk) {
        StringBuilder sb = new StringBuilder();
        sb.append("nomorDokumen=").append(dokumenPk.getNomorDokumen()).append(":kdTransaksi=").append(dokumenPk.getKdTransaksi()).append(":kdJenisTransaksi=").append(dokumenPk.getKdJenisTransaksi()).append(":fgPengganti=").append(dokumenPk.getFgPengganti()).append(":jnsDok=").append(dokumenPk.getJnsDok()).append(":npwpPenjual=").append(dokumenPk.getNpwpPenjual()).append(":namaPenjual=").append(dokumenPk.getNamaPenjual()).append(":npwpPembeli=").append(dokumenPk.getNpwpPembeli()).append(":namaPembeli=").append(dokumenPk.getNamaPembeli()).append(":tanggalDokumen=").append(dokumenPk.getTanggalDokumen()).append(":masaPajak=").append(dokumenPk.getMasaPajak()).append(":tahunPajak=").append(dokumenPk.getTahunPajak()).append(":jumlahDpp=").append(dokumenPk.getJumlahDpp().toPlainString()).append(":jumlahPpn=").append(dokumenPk.getJumlahPpn().toPlainString()).append(":jumlahPpnbm=").append(dokumenPk.getJumlahPpnbm().toPlainString());
        return sb.toString();
    }
}

@Value
class CertificateDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    X509Certificate certificate;
    PrivateKey privateKey;
}

@Service
class CertificateService {

    public CertificateDetails getClientCertificateDetails(String npwp, String passphrase) {
        try {
            File file = new File("/Users/mekari/Documents/Mekari/Efaktur/agent/", npwp + ".p12");
            InputStream inputStream = new FileInputStream(file);
            KeyStore p12 = KeyStore.getInstance("pkcs12");
            p12.load(inputStream, passphrase.toCharArray());
            Enumeration<String> e = p12.aliases();

            String alias;
            do {
                if (!e.hasMoreElements()) {
                    throw new RuntimeException("sertifikat tidak sesuai dengan npwp");
                }

                alias = e.nextElement();
            } while (!alias.contains(npwp));

            X509Certificate certificate = (X509Certificate) p12.getCertificate(alias);
            PrivateKey privateKey = (PrivateKey) p12.getKey(alias, passphrase.toCharArray());
            CertificateDetails certificateDetails = new CertificateDetails(certificate, privateKey);

            try {
                certificate.checkValidity(new Date());
            } catch (CertificateNotYetValidException | CertificateExpiredException var12) {
                throw new RuntimeException("sertifikat tidak valid/expired");
            }

            return certificateDetails;
        } catch (CertificateException | UnrecoverableKeyException | KeyStoreException | IOException | NoSuchAlgorithmException var13) {
            var13.printStackTrace();
            throw new RuntimeException("terjadi kesalahan saat membaca file certificate");
        }
    }
}

@Getter
@Setter
class ReqDokumenPk {
    private String npwpPenjual;
    private String namaPenjual;
    private String npwpPembeli;
    private String namaPembeli;
    private String kdTransaksi;
    private String fgPengganti;
    private String kdJenisTransaksi;
    private String jnsDok;
    private String nomorDokumen;
    private String tanggalDokumen;
    private Byte masaPajak;
    private Short tahunPajak;
    private BigDecimal jumlahDpp;
    private BigDecimal jumlahPpn;
    private BigDecimal jumlahPpnbm;
    private String keterangan;
    private String approvalSign;
    private String signature;
    private String certSn;
    private String nomorDokumenDiganti;
    private String approvalSignDiganti;
}